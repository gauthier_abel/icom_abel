<header>
	<div style="background-color: #66b5c5; padding: 50px; padding-top: 10px; padding-bottom: 20px; margin-bottom: 70px; box-shadow: grey 0px 0px 20px;">
		<ul>
			<li style="display: inline-block; text-align: left; width: 49%;">
				<span style="width: 300;">
					<h1 style="font-size: 50px; color: white;">PROJECT MANAGEMENT</h1>
				</span>
			</li>
			<li style="display: inline-block; text-align: right; width: 49%;">
				<span style="width: 300">
					<img src="assets/img/profile.png" title="swag" alt="profile picture" class="header-image">
				</span>
			</li>
		</ul>
	</div>
</header>