<?php require_once 'inc/config.php';?>

<!DOCTYPE html>
<html>
    <body>

    <?php require_once 'templates/head.php';?>
    <?php require_once 'templates/header.php';?>

    <main>
        <div class="container">
            <ul class="list">
                <li class="table-title">
                    <span class="name">Name</span>
                    <span class="month">01</span>
                    <span class="month">02</span>
                    <span class="month">03</span>
                    <span class="month">04</span>
                    <span class="month">05</span>
                    <span class="month">06</span>
                    <span class="month">07</span>
                    <span class="month">08</span>
                    <span class="month">09</span>
                    <span class="month">10</span>
                    <span class="month">11</span>
                    <span class="month">12</span>
                    <span class="delete"></span>
                </li>

                <?php foreach ($data as $row) :?>
                <li title="Edit task">
                    <span class="name"><?php echo $row['name']?></span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="month">
                        O
                    </span>
                    <span class="delete">
                        <button class="btn-delete" title="Delete task">
                            <a href="delete.php?project=<?php echo $row['id'];?>" class="btn-text" title="Delete project">
                                ❌
                            </a>
                        </button>
                        </a>
                    </span>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
    </main>

    <?php require_once 'templates/footer.php';?>
    </body>
</html>