<?php require_once 'inc/config.php';?>

<!DOCTYPE html>
<html>
    <body>

    <?php require_once 'templates/head.php';?>
    <?php require_once 'templates/header-edit.php';?>

    <main>
        <ul class="editor">
            <li class="edit-item">
                <span>
                    <label class="edit-label">Nom :</label>
                </span>
                <span>
                    <textarea class="edit-name-field"></textarea>
                </span>
            </li>
            <li class="edit-item">
                <span class="edit-label">
                    <label>Started :</label>
                </span>
                <span>
                    <input type="date" class="edit-due-field" name="due-date">
                </span>
            </li>
            <li class="edit-item">
                <span class="edit-label">
                    <label>End :</label>
                </span>
                <span>
                    <input type="date" class="edit-due-field" name="due-date">
                </span>
            </li>
        </ul>
    </main>

    <?php require_once 'templates/footer-edit.php';?>
    </body>
</html>