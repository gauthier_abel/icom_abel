<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Responsive</title>
		<style>
			body{
			    padding: 0 auto;
			}

			header{
			    padding-bottom: 20px;
			}

			h1{
			    font-size: 30px;
			    font-style: bold;
			    margin-left: 20px;
			}

			.container{
			    width: 680px;
			    text-align: center;
			    margin: 0 auto;
			}

			.article-small{
			    width: 140px;
			    height: 140px;
			    margin: 9.99999px;
			    background-color: yellow;
			    display: inline-block;
			}

			.article-big{
			    height: 140px;
			    width: 305px;
			    margin: 9.99999px;
			    background-color: blue;
			    display: inline-block;
			}

			.section{
			    text-align: center;
			}

			@media only screen and (min-width: 660px) { /*640px + 2*10px*/
			    .container {
			        background-color: green;
			    }
			}

			@media only screen and (max-width: 660px) { /*640px + 2*10px*/
			    .container {
			        background-color: red;
			        width: 100%;
			    }

			    .article-small{
			        width: 45%;
			        margin: 2%;
			    }

			    .article-big{
			        margin: 2.5%;
			        width: 95%;
			    }
			}
		</style>

	</head>
	<body>
		<h1 class="header"> Responsive</h1>
		<div class="container">
			<article class="article-small">test</article>
			<article class="article-small">test</article>
			<article class="article-small">test</article>
			<article class="article-small">test</article>
			<article class="article-big">test</article>
			<article class="article-big">test</article>
		</div>
	</body>
</html>